import React from "react";
import { StyleSheet, View, Text, ScrollView, TouchableOpacity } from "react-native";

export default function Botao5({ navigation }) {
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.touchableOpacity}>
                <Text style={{
                    color: 'white', fontSize: 25,
                }}>Vai Curintia!!! </Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        padding: 20,
    },
    touchableOpacity: {
        width: 200,
        height: 200,
        backgroundColor: 'blue',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
    },
});
